import java.util.Date;
import java.util.List;

public class CaloriesCalculator {


    public void forOneMealPerDay(User user, Date date1, MealType mealType, MealRepository mealRepository) {
        calculateCalories(user, date1, mealType, mealRepository);
    }

    public void calculateForAllMeals(User user, MealType mealType, MealRepository mealRepository) {
        calculateCalories(user, mealType, mealRepository);
    }

    public void forAlltime(User user, MealRepository mealRepository) {
        calculateCalories(user, mealRepository);
    }

    public void perDay(User user, Date date1, MealRepository mealRepository) {
        calculateCalories(user, date1, mealRepository);
    }

    public void perDayTestWithFood(User user, MealRepository mealRepository) {
        calculateCalories(user, mealRepository);
    }

    private void calculateCalories(User user, MealRepository mealRepository) {
        int calories = 0;
        int fats = 0;
        int proteins = 0;
        int carbohydrates = 0;
        for (Meal meal : mealRepository.getMeals()) {
            if (meal.getUser().equals(user)) {
                for (Food food : meal.getMealFoods()) {
                    calories += food.getCalories();
                    fats += food.getFats();
                    proteins += food.getProteins();
                    carbohydrates += food.getCarbohydrates();
                }
            }
        }
        if (calories != 0) {
            System.out.println("Sum all foods for all time ");
            System.out.println("Calories " + calories + " fats  " + fats + " proteins  " + proteins + " carbohydrates  " + carbohydrates);
        } else {
            System.out.println("We have not food");
        }
    }

    private void calculateCalories(User user, Date date1, MealType mealType, MealRepository mealRepository) {
        int calories = 0;
        int fats = 0;
        int proteins = 0;
        int carbohydrates = 0;
        for (Meal meal : mealRepository.getMeals()) {
            if (meal.getUser().equals(user) &&
                    meal.getTypeOfMeal().equals(mealType) &&
                    meal.getDateOfMeal().equals(date1)) {
                for (Food food : meal.getMealFoods()) {
                    calories += food.getCalories();
                    fats += food.getFats();
                    proteins += food.getProteins();
                    carbohydrates += food.getCarbohydrates();
                }
            }
        }
        if (calories != 0) {
            System.out.println("Sum all foods for  " + mealType);
            System.out.println("Calories " + calories + " fats  " + fats + " proteins  " + proteins + " carbohydrates  " + carbohydrates);
        } else {
            System.out.println("We have not food");
        }
    }

    private void calculateCalories(User user, MealType supper, MealRepository mealRepository) {
        int calories = 0;
        int fats = 0;
        int proteins = 0;
        int carbohydrates = 0;
        for (Meal meal : mealRepository.getMeals()) {
            if (meal.getUser().equals(user) && meal.getTypeOfMeal().equals(supper)) {
                for (Food food : meal.getMealFoods()) {
                    calories += food.getCalories();
                    fats += food.getFats();
                    proteins += food.getProteins();
                    carbohydrates += food.getCarbohydrates();
                }
            }
        }
        if (calories != 0) {
            System.out.println("Sum all foods for  " + supper);
            System.out.println("Calories " + calories + " fats  " + fats + " proteins  " + proteins + " carbohydrates  " + carbohydrates);
        } else {
            System.out.println("We have not food");
        }
    }

    private void calculateCalories(User user, Date date1, MealRepository mealRepository) {
        int calories = 0;
        int fats = 0;
        int proteins = 0;
        int carbohydrates = 0;
        for (Meal meal : mealRepository.getMeals()) {
            if (meal.getUser().equals(user) && meal.getDateOfMeal().equals(date1)) {
                for (Food food : meal.getMealFoods()) {
                    calories += food.getCalories();
                    fats += food.getFats();
                    proteins += food.getProteins();
                    carbohydrates += food.getCarbohydrates();
                }
            }
        }
        if (calories != 0) {
            System.out.println("Sum all foods for day " + date1.toString());
            System.out.println("Calories " + calories + " fats  " + fats + " proteins  " + proteins + " carbohydrates  " + carbohydrates);
        } else {
            System.out.println("We have not food");
        }
    }

    public MealInformation calculateSumOfMeal(Meal meal) {
        MealInformation mealInformation = new MealInformation();
        mealInformation.setTypeOfMeal(meal.getTypeOfMeal());
        for (Food food : meal.getMealFoods()) {
            mealInformation.setCalories(mealInformation.getCalories() + food.getCalories());
            mealInformation.setFats(mealInformation.getFats() + food.getFats());
            mealInformation.setProteins(mealInformation.getProteins() + food.getProteins());
            mealInformation.setCarbohydrates(mealInformation.getCarbohydrates() + food.getCarbohydrates());
        }
        if (mealInformation.getCalories() <= 0)
            mealInformation = null;

        return mealInformation;
    }

    public int calculateCalories(Meal meal) {
        int allCallories = 0;
        for (Food food : meal.getMealFoods()) {
            allCallories += food.getCalories();
        }
        return allCallories;
    }

    public int calculateFats(Meal meal) {
        int allCallories = 0;
        for (Food food : meal.getMealFoods()) {
            allCallories += food.getFats();
        }
        return allCallories;
    }

    public int calculateProteins(Meal meal) {
        int allCallories = 0;
        for (Food food : meal.getMealFoods()) {
            allCallories += food.getProteins();
        }
        return allCallories;
    }

    public int calculateCarbohydrates(Meal meal) {
        int allCallories = 0;
        for (Food food : meal.getMealFoods()) {
            allCallories += food.getCarbohydrates();
        }
        return allCallories;
    }

    public void showMealFoodsAndComponents(List<Meal> meals) {
        if (meals == null) {
            return;
        }
        for (Meal meal : meals) {
            System.out.println(meal.getTypeOfMeal());
            for (Food food : meal.getMealFoods()) {
                System.out.println("Name of Food: " + food.getName() +
                        " Fats " + food.getFats() +
                        " Calories " + food.getCalories());
            }
            System.out.println(calculateSumOfMeal(meal));
        }
    }

    public void showMealFoodsAndComponentsLikeAList(List<Meal> meals) {
        if (meals == null) {
            return;
        }
        for (Meal meal : meals) {
            System.out.println("---------------------------");
            System.out.println("|" + meal.getTypeOfMeal() + "|");
            System.out.println("---------------------------");
            for (Food food : meal.getMealFoods()) {
                System.out.println("Name of Food: " + food.getName() +
                        " Fats " + food.getFats() +
                        " Calories " + food.getCalories());
            }
            System.out.println(calculateSumOfMeal(meal));
        }
    }

    public void showMealFoodsAndComponentsLikeTable(List<Meal> meals) {
        int maxSizeOfColum = 0;
        if (meals == null) {
            return;
        }
        maxSizeOfColum = meals.get(0).getMealFoods().size();

        System.out.println("----------------------------------------------------");
        for (int i = 0; i < meals.size(); i++) {
            System.out.print("|" + meals.get(i).getTypeOfMeal() + "|" + '\t');
            if(maxSizeOfColum < meals.get(i).getMealFoods().size()){
                maxSizeOfColum = meals.get(i).getMealFoods().size();
            }
        }

        System.out.println();
        System.out.println("----------------------------------------------------");

        for (int j = 0; j < maxSizeOfColum; j++) {
            for (int i = 0; i < meals.size(); i++) {
                if(meals.get(i).getMealFoods().size() > j && meals.get(i)!=null){
                    System.out.print("  " + meals.get(i).getMealFoods().get(j).getName()+ "  ");
                }
                else{
                    System.out.print(" ***** ");
                }
            }
            System.out.println();
        }

        for (Meal meal : meals) {
            System.out.print("|" + calculateCalories(meal) + "|" + '\t');
        }
    }
}
