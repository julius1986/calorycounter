/**
 * Created by julius on 04.06.2015.
 */
public enum HtmlPageConstant {
    HEADER("\n" +
            "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"\n" +
            "\"http://www.w3.org/TR/html4/loose.dtd\">\n" +
            "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
            "\n" +
            "<head>\n" +
            "<title>Test</title>\n" +
            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
            "\n" +
            "  <link rel=\"stylesheet\" type=\"text/css\" href=\"mystyle.css\">\n" +
            "  \n" +
            "</head> \n" +
            "\n" +
            "<body>"),

    FOOTER("\n" +
            "</body>\n" +
            "\n" +
            "</html>");
    private final String text;

    public String getTextPage() {
        return text;
    }

    HtmlPageConstant(String text){
        this.text = text;
    }

}
