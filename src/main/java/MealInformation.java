
public class MealInformation {
    private int calories;
    private int fats;
    private int proteins;
    private int carbohydrates;
    private MealType typeOfMeal;

    public MealInformation() {
        this.calories = 0;
        this.fats = 0;
        this.proteins = 0;
        this.carbohydrates = 0;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public int getFats() {
        return fats;
    }

    public void setFats(int fats) {
        this.fats = fats;
    }

    public int getProteins() {
        return proteins;
    }

    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    public int getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(int carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public MealType getTypeOfMeal() {
        return typeOfMeal;
    }

    public void setTypeOfMeal(MealType typeOfMeal) {
        this.typeOfMeal = typeOfMeal;
    }

    @Override
    public String toString() {
        return "MealInformation{" +
                "Name of meal=" + typeOfMeal +
                ", carbohydrates=" + carbohydrates +
                ", calories=" + calories +
                ", fats=" + fats +
                ", proteins=" + proteins +
                '}';
    }
}