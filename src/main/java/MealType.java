
public enum MealType {
    BREAKFAST,
    LUNCH,
    DINNER,
    SUPPER,
    OTHER
}
