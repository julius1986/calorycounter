import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {


    public static void main(String[] args) throws Exception {
        User user = new User();
        MealRepository mealRepository = new MealRepository();//All meals of Users
        CaloriesCalculator caloriesCalculator = new CaloriesCalculator();//Class helper for calculation
        CommonFoodCatalog commonFoodCatalog = new CommonFoodCatalog();//Catalog of all foods
        Meal currentMeal = new Meal(user, MealType.BREAKFAST, new Date());
        Date date = new Date();
        List<Meal> testMeal = new ArrayList<Meal>();
        FileWorker htmlFile = new FileWorker();
        HtmlCreator htmlCreator = new HtmlCreator();






        commonFoodCatalog.addNewFood(new Food(15,1,223,"Banan"));
        commonFoodCatalog.addNewFood(new Food(11,15,44,"Tikva"));
        commonFoodCatalog.addNewFood(new Food(111,23,63,"Bread"));
        commonFoodCatalog.addNewFood(new Food(111,233,63,"Sosiski"));
        commonFoodCatalog.addNewFood(new Food(800,223,633,"Oil"));

        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Banan"));
        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Banan"));
        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Banan"));
        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Tikva"));
        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Banan"));
        testMeal.add(currentMeal);

        mealRepository.addNewMeal(currentMeal);

        currentMeal.showFoodsOfMeal();



        currentMeal = new Meal(user, MealType.DINNER);

        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Tikva"));
        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Sosiski"));
        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Oil"));
        testMeal.add(currentMeal);

        mealRepository.addNewMeal(currentMeal);

        currentMeal.showFoodsOfMeal();



        currentMeal = new Meal(user, MealType.SUPPER);

        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Bread"));
        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Tikva"));
        currentMeal.addFoodToMeal(commonFoodCatalog.getFood("Bread"));
        testMeal.add(currentMeal);

        mealRepository.addNewMeal(currentMeal);

        mealRepository.showFoodFromMeals(MealType.SUPPER);

        caloriesCalculator.forAlltime(user, mealRepository);

        caloriesCalculator.perDayTestWithFood(user, mealRepository);



        List<Meal> meals = mealRepository.getByUserAndDate(user, date);

        caloriesCalculator.showMealFoodsAndComponents(meals);
        caloriesCalculator.showMealFoodsAndComponentsLikeAList(testMeal);
        caloriesCalculator.showMealFoodsAndComponentsLikeTable(testMeal);
        htmlFile.createFile("table.html", testMeal);

        System.out.println(htmlCreator.getHtmlText());
        htmlCreator.openTable();
        htmlCreator.openTr();



        htmlCreator.closeTr();
        htmlCreator.closeTable();
        System.out.println(htmlCreator.getHtmlText());

    }

}

