
public class Food {
    private int calories;
    private int fats;
    private int proteins;
    private int carbohydrates;
    private String name;

    public Food() {
        this.calories = 0;
        this.fats = 0;
        this.proteins = 0;
        this.carbohydrates = 0;
    }



    public int getCalories() {
        return calories;
    }

    public String getName() {
        return name;
    }

    public int getFats() {
        return fats;
    }

    public int getProteins() {
        return proteins;
    }

    public int getCarbohydrates() {
        return carbohydrates;
    }

    public Food( int fats, int proteins, int carbohydrates, String name) {
        this.fats = fats;
        this.proteins = proteins;
        this.carbohydrates = carbohydrates;
        this.name = name;
        this.calories = CaloriesPerProductType.FAT.getCalories() * fats +
                CaloriesPerProductType.CARBOHYDRAT.getCalories() * carbohydrates +
                CaloriesPerProductType.PROTEIN.getCalories() * proteins;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setFats(int fats) {
        this.fats = fats;
    }

    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    public void setCarbohydrates(int carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public void setName(String name) {
        this.name = name;
    }

}
