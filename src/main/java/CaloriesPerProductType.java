/**
 * Created by julius on 22.05.2015.
 */
public enum CaloriesPerProductType {
    FAT(8),
    PROTEIN(4),
    CARBOHYDRAT(4);
    private final int calories;

    public int getCalories() {
        return calories;
    }

    CaloriesPerProductType(int calories){
        this.calories = calories;
    }
}
