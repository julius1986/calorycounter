import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FileWorker {
    File htmFile = null;
    String pageText;


   public FileWorker() throws IOException {

    }

    public void createFile(String name, List<Meal> testMeal){
        htmFile = new File(name);
        try {
            //проверяем, что если файл не существует то создаем его
            if(!htmFile.exists()){
                htmFile.createNewFile();
            }

            //PrintWriter обеспечит возможности записи в файл
            PrintWriter out = new PrintWriter(htmFile.getAbsoluteFile());
            HtmlCreator htmlCreator = new HtmlCreator();
            try {
                //Записываем текст у файл
                htmlCreator.createHtmlTable(testMeal);
                pageText = HtmlPageConstant.HEADER.getTextPage() + htmlCreator.getHtmlText() +  HtmlPageConstant.FOOTER.getTextPage();
                htmlCreator.resetHtmlText();
                out.print(pageText);
            } finally {
                //После чего мы должны закрыть файл
                //Иначе файл не запишется
                out.close();
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }



}
