import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Meal {
    private Date dateOfMeal;
    private MealType typeOfMeal;
    private List<Food> mealFoods = new ArrayList<Food>();
    private User user;

    public List<Food> getMealFoods() {
        return mealFoods;
    }

    public User getUser() {

        return user;
    }

    public Date getDateOfMeal() {
        return dateOfMeal;
    }

    public Meal(User user, MealType typeOfMeal) {
        this.dateOfMeal = new Date();
        this.typeOfMeal = typeOfMeal;
        this.user = user;
    }

    public Meal(User user, MealType typeOfMeal, Date date) {
        this.dateOfMeal = date;
        this.typeOfMeal = typeOfMeal;
        this.user = user;

    }

    public void addFoodToMeal(Food food){
        mealFoods.add(food);
    }

    public MealType getTypeOfMeal() {
        return typeOfMeal;
    }

    public void showFoodsOfMeal() {
        for(Food food:mealFoods){
            System.out.println("Name of food: " + food.getName()+ "\n"
                            + "  Calories: " + food.getCalories()
                            + " Proteins: "+ food.getProteins()
                            + " Fats: "+ food.getFats()
                            + " Carbohydrates: "+ food.getCarbohydrates() +"\n"
            );
        }
    }



}