import java.util.List;

public class HtmlCreator {
    private String htmlText = new String();

    public String createHtmlTable(List<Meal> meals) {
        CaloriesCalculator caloriesCalculator = new CaloriesCalculator();
        String textTable = "<table>";

        textTable += tHeadCreator(meals);
        textTable += tBodyCreator(meals);
        textTable += tFootCreator(meals);

        textTable += "</table>";
        htmlText = textTable;
        return htmlText;
    }

    private String tFootCreator(List<Meal> meals) {
        String textTable = new String();
        resetHtmlText();
        openTfoot();
        CaloriesCalculator caloriesCalculator = new CaloriesCalculator();
        openTr("row");
        for (Meal meal : meals) {
            MealInformation mealInformation = caloriesCalculator.calculateSumOfMeal(meal);
            createTd("colum","Итог");
            createTd("colum",mealInformation.getProteins());
            createTd("colum",mealInformation.getFats());
            createTd("colum",mealInformation.getCarbohydrates());
            createTd("colum",mealInformation.getCalories());
        }
        closeTr();
        closeTfoot();
        textTable += getHtmlText();
        return textTable;
    }

    private String tBodyCreator(List<Meal> meals) {
        String textTable = new String();
        resetHtmlText();
        openTbody();
        int maxSizeOfColum = countMaxAmountOfFoods(meals);
        for (int j = 0; j < maxSizeOfColum; j++) {
            openTr("row");
            for (int i = 0; i < meals.size(); i++) {
                if(meals.get(i).getMealFoods().size() > j && meals.get(i)!=null){
                    createTd("colum", meals.get(i).getMealFoods().get(j).getName());
                    createTd("colum", meals.get(i).getMealFoods().get(j).getProteins());
                    createTd("colum", meals.get(i).getMealFoods().get(j).getFats());
                    createTd("colum", meals.get(i).getMealFoods().get(j).getCarbohydrates());
                    createTd("colum", meals.get(i).getMealFoods().get(j).getCalories());
                }
                else{
                    createTd("colum","");
                    createTd("colum","");
                    createTd("colum","");
                    createTd("colum","");
                    createTd("colum","");
                }
            }
            closeTr();
        }
        closeTbody();
        textTable += getHtmlText();
        return textTable;
    }

    private String tHeadCreator(List<Meal> meals) {
        String textTable = new String();
        resetHtmlText();
        openThead();
        openTr("row");
        int maxSizeOfColum = countMaxAmountOfFoods(meals);
        for (int i = 0; i < meals.size(); i++) {
            createTd("colum", 5, meals.get(i).getTypeOfMeal().toString());
            if(maxSizeOfColum < meals.get(i).getMealFoods().size()){
                maxSizeOfColum = meals.get(i).getMealFoods().size();
            }
        }
        closeTr();

        openTr("row");
        for (int i = 0; i < meals.size(); i++){

            createTd("colum", "Название");
            createTd("colum", "Белки");
            createTd("colum", "Жиры");
            createTd("colum", "Углеводы");
            createTd("colum", "Калории");

        }
        closeTr();
        closeThead();
        textTable +=getHtmlText();
        return textTable;
    }

    private int countMaxAmountOfFoods(List<Meal> meals) {
        int maxSizeOfColum = meals.get(0).getMealFoods().size();
        for (int i = 0; i < meals.size(); i++) {
            if(maxSizeOfColum < meals.get(i).getMealFoods().size()){
                maxSizeOfColum = meals.get(i).getMealFoods().size();
            }
        }
        return maxSizeOfColum;
    }
    public void resetHtmlText() {
        htmlText = new String();
    }
    public String getHtmlText() {
        return htmlText;
    }


    public void openTable(){
        htmlText += "<table>\n";
    }
    public void closeTable(){
        htmlText += "</table>\n";
    }
    public void openThead(){
        htmlText += "<thead>\n";
    }
    public void closeThead(){
        htmlText += "</thead>\n";
    }
    public void openTbody(){
        htmlText += "<tbody>\n";
    }
    public void closeTbody(){
        htmlText += "</tbody>\n";
    }

    public void openTfoot(){
        htmlText += "<tfoot>\n";
    }
    public void closeTfoot(){
        htmlText += "</tfoot>\n";
    }

    public void openTr(){
        htmlText += "<tr>\n";
    }
    public void closeTr(){
        htmlText += "</tr>\n";
    }
    public void openTr(String className){
        htmlText += "<tr class = \"" + className + "\">\n";
    }
    public void openTr(int colspan){
        htmlText += "<tr colspan=\""+ colspan +"\">\n";
    }
    public void openTr(String className, int colspan){
        htmlText += "<tr colspan=\""+colspan+"\" class = \""+className+"\">\n";
    }

    public void createTd(String text){
        htmlText += "<td>" + text + "</td>\n";
    }
    public void createTd(String className, String text){
        htmlText += "<td class = \"" + className + "\">"+ text +"</td>\n";
    }
    public void createTd(int colspan, String text){
        htmlText += "<td colspan=\""+ colspan +"\">"+ text +"</td>\n";
    }
    public void createTd(int colspan, int text){
        htmlText += "<td colspan=\""+ colspan +"\">"+ text +"</td>\n";
    }
    public void createTd(String className, int text){
        htmlText += "<td class = \""+ className +"\">"+ text +"</td>\n";
    }
    public void createTd(String className, int colspan, String text){
        htmlText += "<td colspan=\""+colspan+"\" class = \""+className+"\">"+ text +"</td>\n";
    }
    public void createTd(String className, int colspan, int text){
        htmlText += "<td colspan=\""+colspan+"\" class = \""+className+"\">"+ text +"</td>\n";
    }

    public void createEmptyTd(String className, int colspan){
        htmlText += "<td colspan=\""+colspan+"\" class = \""+className+"\"></td>\n";
    }
    public void createEmptyTd(String className){
        htmlText += "<td class = \""+className+"\"></td>\n";
    }
    public void createEmptyTd(int colspan){
        htmlText += "<td colspan=\""+colspan+"\"></td>\n";
    }

    public void takeText(String text){
        htmlText += text;
    }
    public void takeText(int text){
        htmlText += Integer.toString(text);
    }
    public void takeText(Object o){
        htmlText += o.toString();
    }

}
