import java.util.HashMap;
import java.util.Map;

public class CommonFoodCatalog {
    private Map<String,Food> foods = new HashMap<String, Food>();

    public void addNewFood(Food food){
            foods.put(food.getName(), food);
    }

    public Food getFood(String name) {
        if (!foods.containsKey(name)) {
            throw new IllegalStateException("There is no food with such name");
        }
        return foods.get(name);
    }

}
