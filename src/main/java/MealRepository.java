import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MealRepository {
    private List<Meal> meals = new ArrayList<Meal>();

    public List<Meal> getMeals() {
        return meals;
    }

    public void addNewMeal(Meal meal){
        meals.add(meal);
    }

    public void showFoodFromMeals(MealType typeOfMeal){
        for(Meal meal: meals){
            if(meal.getTypeOfMeal().equals(typeOfMeal)) {
                System.out.println(meal.getTypeOfMeal());
                meal.showFoodsOfMeal();
            }
        }
    }

    public List<Meal> getByUserAndDate(User user, Date date) {
        List<Meal> tempMeals = new ArrayList<Meal>();
        for(Meal meal: meals){
            if(meal.getUser().equals(user) && meal.getDateOfMeal().equals(date)) {
                tempMeals.add(meal);
            }
        }

        return tempMeals;
    }
}
